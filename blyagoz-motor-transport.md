#Виртуальная лаборатория для проверки навыков определения неисправностей автотранспорта 

##Сведения об учащемся 
Блягоз Софиет Хазретовна, группа P42211 

##Краткое описание 
Проверка навыков определения неисправностей автотранспорта включает в себя проведение тестирования: дан список неисправностей, которые необходимо соотнести с соответствующими им блокам поиска неисправностей.

##Аналоги 
В интернете не удалось найти сайты и приложения, которые занимаются этим. Также не удалось найти виртуальные аналоги.

##Детали реализации: 
Нотация для проектирования: UML
